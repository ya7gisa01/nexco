﻿using SQLite4Unity3d;

public class package {

	[PrimaryKey, AutoIncrement]

	public int pid { get; set; }
	public int q1 { get; set; }
	public int q2 { get; set; }
	public int q3 { get; set; }
	public int q4 { get; set; }
	public int q5 { get; set; }
	public string comment { get; set; }
	public string difficulty { get; set; }


	public override string ToString ()
	{
		//return string.Format ("[Person: Id={0}, Name={1},  Surname={2}, Age={3}]", Id, Name, Surname, Age);
		return string.Format ("[quiz: pid={0}, q1={1}, q2={2}, q3={3}, q4={4}, q5={5}, comment={6}, difficulty={7}]",
			pid, q1, q2, q3, q4, q5, comment, difficulty);
	}
}
