﻿using SQLite4Unity3d;

public class quiz {

	[PrimaryKey, AutoIncrement]
	//public int Id { get; set; }
//	public string Name { get; set; }
//	public string Surname { get; set; }
//	public int Age { get; set; }

	public int id { get; set; }
	public string category { get; set; }
	public string body { get; set; }
	public string answer { get; set; }
	public string choiceA { get; set; }
	public string choiceB { get; set; }
	public string choiceC { get; set; }
	public string commentary { get; set; }
	public string sourcefrom { get; set; }
	public string difficulty { get; set; }

	public override string ToString ()
	{
		//return string.Format ("[Person: Id={0}, Name={1},  Surname={2}, Age={3}]", Id, Name, Surname, Age);
		return string.Format ("[quiz: id={0}, category={1}, body={2}, answer={3}, choiceA={4}, choiceB={5}, choiceC={6}, commentary={7}, sourcefrom={8}, difficulty={9}]",
			id, category, body, answer, choiceA, choiceB, choiceC, commentary, sourcefrom, difficulty);
	}
}
