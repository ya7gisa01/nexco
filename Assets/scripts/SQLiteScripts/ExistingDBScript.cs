﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ExistingDBScript : MonoBehaviour {
	// text for fix..
	string fixedTextOfQ5 = "<ruby='かこ'>過去</ruby>の<ruby='じゅうたい'>渋滞</ruby>データなどから、どこでどのくら<ruby='　　じゅうたい'>い渋</ruby>滞が<ruby='はっせい'>発生</ruby>するかを<ruby='よそく'>予測</ruby>しているよ。<ruby='てきちゅうりつ'>的中率</ruby>８０％<ruby='いじょう'>以上</ruby>！<ruby='よほう'>予報</ruby>をチェックし<ruby='　　じゅうたい'>、渋滞</ruby>を<ruby='さ'>避</ruby>けて<ruby='きも'>気持</ruby>ちよくドライブ";
	string fixedTextOfQ19 = "<ruby='いちばんみぎがわ'>一番右側</ruby>を「<ruby='お'>追</ruby>い<ruby='こ'>越</ruby>し<ruby='しゃせん'>車線</ruby>」と<ruby='い'>言</ruby>います。<ruby='<color=#00000000>a</color>お'>追</ruby>い<ruby='<color=#00000000>a</color>こ'>越</ruby>す<ruby='ばあい'>場合</ruby>は<ruby='みぎがわ'>右側</ruby>から。";
	string fixedTextOfQ20 = "<ruby='まえ'>前</ruby>の<ruby='くるま'>車</ruby>を<ruby='お'>追</ruby>い<ruby='こ'>越</ruby>すため、<ruby='おいこししゃせん'>追越車線</ruby>から<ruby='<color=#00000000>a</color>お'>追</ruby>い<ruby='<color=#00000000>a</color>こ'>越</ruby>しをした<ruby='あと'>後</ruby>にすることは？";
	string fixedTextOfQ40 = "<ruby='あか'>明</ruby>るい<ruby='ところ'>所</ruby>から<ruby='きゅう'>急</ruby><ruby='<color=#00000000>aaaa</color>くら'>に暗</ruby><ruby='<color=#00000000>aaaa</color>ところ'>い所</ruby>に<ruby='い'>行</ruby>くと、<ruby='め'>目</ruby>が<ruby='な'>慣</ruby>れずによく<ruby='み'>見</ruby>えないことがあるんだ。<ruby='いりぐち'>入口</ruby>の<ruby='しょうめい'>照明</ruby>を<ruby='あか'>明</ruby>るくして<ruby='じょじょ'>徐々</ruby>に<ruby='くら'>暗</ruby>くすることで、<ruby='め'>目</ruby>が<ruby='な'>慣</ruby>れるよう<ruby='くふう'>工夫</ruby>してるんだよ。";

	//public Text DebugText;
	int q1 = 0, q2 = 0, q3 = 3, q4 = 0, q5 = 0;
	// Use this for initialization
	void Start () {
		//var ds = new DataService ("existing.db");
		var ds = new DataService ("nexco.db3", Downloader.bytesAndroid, Downloader.isAndroidWWW);
		//ds.CreateDB ();
		//var people = ds.GetPersons ();
		var quizes = ds.GetQuizes ();
		ToConsole (quizes);
		//ToConsole (people);

		int targetId = getPackageId ();
		// for debug
		//int targetId = survive.packageForDebug;
		var indices = ds.GetIndicesOfSpecificPackage (targetId);
		ToConsoleIndex (indices);

		var quizA = ds.GetSpecificQuiz (q1);
		var quizB = ds.GetSpecificQuiz (q2);
		var quizC = ds.GetSpecificQuiz (q3);
		var quizD = ds.GetSpecificQuiz (q4);
		var quizE = ds.GetSpecificQuiz (q5);

		// set data
		setQuizObj(quizA, 0, targetId);
		setQuizObj(quizB, 1, targetId);
		setQuizObj(quizC, 2, targetId);
		setQuizObj(quizD, 3, targetId);
		setQuizObj(quizE, 4, targetId);

	}
	
	//private void ToConsole(IEnumerable<Person> people){
	private void ToConsole(IEnumerable<quiz> quizes){
		foreach (var person in quizes) {
			ToConsole(person.ToString());
		}
	}

	void setQuizObj(IEnumerable<quiz> quizes, int arrIndex, int targetPackage){
		foreach (var quiz in quizes) {
			//GameController.QuizObj quiz = new GameController.QuizObj(idInt, questionText, answer, choiceA, choiceB, choiceC, commentary, orderInt, packageIdInt, mode);
			GameController.quizObjs [arrIndex] = new GameController.QuizObj(quiz.id, quiz.body, quiz.answer, quiz.choiceA, quiz.choiceB, quiz.choiceC, quiz.commentary, 0, 0, survive.selectedLevel.ToString());
		}
		// if quizid is what we really cared about, fix some values
		if (
			targetPackage == 2 &&
			arrIndex == 2 &&
			GameController.quizObjs[2].commentary == "<ruby='かこ'>過去</ruby>の<ruby='じゅうたい'>渋滞</ruby>データなどから、どこでどのくら<ruby='じゅうたい'>い渋</ruby>滞が<ruby='はっせい'>発生</ruby>するかを<ruby='よそく'>予測</ruby>しているよ。<ruby='てきちゅうりつ'>的中率</ruby>８０％<ruby='いじょう'>以上</ruby>！<ruby='よほう'>予報</ruby>をチェックし、<ruby='じゅうたい'>渋滞</ruby>を<ruby='さ'>避</ruby>けて<ruby='きも'>気持</ruby>ちよくドライブ"
		) {
			GameController.quizObjs [arrIndex].commentary = fixedTextOfQ5;
		}
		if (targetPackage == 4 &&
			arrIndex == 2 &&
			GameController.quizObjs[2].commentary == "<ruby='かこ'>過去</ruby>の<ruby='じゅうたい'>渋滞</ruby>データなどから、どこでどのくら<ruby='じゅうたい'>い渋</ruby>滞が<ruby='はっせい'>発生</ruby>するかを<ruby='よそく'>予測</ruby>しているよ。<ruby='てきちゅうりつ'>的中率</ruby>８０％<ruby='いじょう'>以上</ruby>！<ruby='よほう'>予報</ruby>をチェックし、<ruby='じゅうたい'>渋滞</ruby>を<ruby='さ'>避</ruby>けて<ruby='きも'>気持</ruby>ちよくドライブ"
		) {
			GameController.quizObjs [arrIndex].commentary = fixedTextOfQ5;
		}
		if (targetPackage == 5 &&
			arrIndex == 2 &&
			GameController.quizObjs[2].commentary == "<ruby='いちばんみぎがわ'>一番右側</ruby>を「<ruby='お'>追</ruby>い<ruby='こ'>越</ruby>し<ruby='しゃせん'>車線</ruby>」と<ruby='い'>言</ruby>います。<ruby='お'>追</ruby>い<ruby='こ'>越</ruby>す<ruby='ばあい'>場合</ruby>は<ruby='みぎがわ'>右側</ruby>から。"
		) {
			GameController.quizObjs [arrIndex].commentary = fixedTextOfQ19;
		}
		if (targetPackage == 6 &&
			arrIndex == 2 &&
			GameController.quizObjs[2].questionText == "<ruby='まえ'>前</ruby>の<ruby='くるま'>車</ruby>を<ruby='お'>追</ruby>い<ruby='こ'>越</ruby>すため、<ruby='おいこししゃせん'>追越車線</ruby>から<ruby='お'>追</ruby>い<ruby='こ'>越</ruby>しをした<ruby='あと'>後</ruby>にすることは？"
		) {
			GameController.quizObjs [arrIndex].questionText = fixedTextOfQ20;
		}
		if (targetPackage == 10 &&
			arrIndex == 4 &&
			GameController.quizObjs[4].commentary == "<ruby='あか'>明</ruby>るい<ruby='ところ'>所</ruby>から<ruby='きゅう'>急</ruby><ruby='くら'>に暗</ruby><ruby='ところ'>い所</ruby>に<ruby='い'>行</ruby>くと、<ruby='め'>目</ruby>が<ruby='な'>慣</ruby>れずによく<ruby='み'>見</ruby>えないことがあるんだ。<ruby='いりぐち'>入口</ruby>の<ruby='しょうめい'>照明</ruby>を<ruby='あか'>明</ruby>るくして<ruby='じょじょ'>徐々</ruby>に<ruby='くら'>暗</ruby>くすることで、<ruby='め'>目</ruby>が<ruby='な'>慣</ruby>れるよう<ruby='くふう'>工夫</ruby>してるんだよ。"
		) {
			GameController.quizObjs [arrIndex].commentary = fixedTextOfQ40;	
		}
	}

	private void ToConsoleIndex(IEnumerable<package> indices){
		foreach (var index in indices) {
			q1 = index.q1;
			q2 = index.q2;
			q3 = index.q3;
			q4 = index.q4;
			q5 = index.q5;

			ToConsole (index.ToString ());
		}
	}

	private void ToConsole(string msg){
		//DebugText.text += System.Environment.NewLine + msg;
		Debug.Log (msg);
	}

	public static int getPackageId(){
		int level = survive.selectedLevel;
		int pId=0;
		if (level == 1 || level == 0) {
			pId = UnityEngine.Random.Range (1, 4);
		} else if (level == 2) {
			pId = UnityEngine.Random.Range (5, 8);
		} else if (level == 3) {
			pId = UnityEngine.Random.Range (9, 12);
		}
		Debug.Log (pId.ToString());
		return pId;
	}

}
