﻿using UnityEngine;
using System.Collections;

public class TiresAnimationManager : MonoBehaviour {


	private GameObject carObj;
	private Transform frontLeft;
	private Transform frontRight;
	private Transform rearLeft;
	private Transform rearRight;

	// Use this for initialization
	void Start () {
		carObj = GameObject.Find ("NEXCO_carmodel_0206_01");
		// set each private variable.
		frontLeft = GameObject.Find("tire_front_L").transform;
		frontRight = GameObject.Find ("tire_front_R").transform;
		rearLeft = GameObject.Find ("tire_rear_L").transform;
		rearRight = GameObject.Find ("tire_rear_R").transform;
	
	}
	
	// Update is called once per frame
	void Update () {
		rotateTires ();
	}

	void rotateTires(){
		//this.transform.Find ("tire_front_L").transform.Rotate(Time.deltaTime * 900, 0, 0);
		//this.transform.Find ("tire_rear_L").transform.Rotate(Time.deltaTime * 900, 0, 0);
		//this.transform.Find ("tire_front_R").transform.Rotate(Time.deltaTime * 900, 0, 0);
		//this.transform.Find ("tire_rear_R").transform.Rotate(Time.deltaTime * 900, 0, 0);
		frontLeft.Rotate(Time.deltaTime * 900, 0, 0);
		frontRight.Rotate(Time.deltaTime * 900, 0, 0);
		rearLeft.Rotate(Time.deltaTime * 900, 0, 0);
		rearRight.Rotate(Time.deltaTime * 900, 0, 0);
	}
}
