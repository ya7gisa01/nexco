﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
// use RegEx in C#
using System.Text.RegularExpressions;


public class GameController : MonoBehaviour {
	// quite unsure how to code well with Unity. 
	// obviously need to deply some design pattern for further development..

	public string url = "dev.naked-inc.com/json.php";
	public string carComponentName;

	int currentQuizIndex = 0;

	// many timers, many booleans
	bool isInstructionReady = false;
	bool isQuestionTextReady = false;
	bool isChoicesTextReady = false;
	bool isAnswerReady = false;
	bool isCommentaryTextReady = false;

	//bool isCarChangingLane = false;
	bool isCarChangingLaneToLeftFromCenter = false;
	bool isCarChangingLaneToRightFromCenter = false;
	bool isCarChangingLaneToCenterFromLeft = false;
	bool isCarChangingLaneToCenterFromRight = false;


	public Material defaultMat;
	public Material redLampMat;
	public Material EmissionMat;
	public Material lampRightMat;
	public Material lampLeftMat;

	public Color redMatColor;
	public Color yellowMatColor;

	Color transparentWhite;

	// timer animation
	bool isTimerAnimationOn = false;
	float timerStartTime;

	float fadeStartTime;

	// boolean to check if the animation of winker triggers or not
	bool isInitialAnimationCall = true;

	// maybe better to store the car game object to a variable, whose type is "GameObject"
	private GameObject carObj;

	// maybe we need arrays to store data..
	public static QuizObj[] quizObjs = new QuizObj[5];

	// private IEnumerators to store info, probably need to reset timer stuff as well
	private IEnumerator instructionCoroutine;

	// UIs.. to be faded out.
	public Button quizStartBtn;
	public Button quitGameBtn;
	public GameObject nextBtn;

	// booleans for fading animations
	bool isQuizStartBtnFading = false;

	// and animator!
	public Animator anim;

	// const.
	const float fixedVisibleScale = 0.9f;

	// buttons
	public GameObject[] buttons = new GameObject[5];
	// booleans..
	private bool isQuestionNumFadingIn = false;
	private bool isQuestionNumFadingOut = false;

	// another float to store the inital log of the fading in/out animation
	private float FadeStartTimeOfQuestionNum;

	public AudioClip okSound;
	public AudioClip ngSound;
	public AudioClip commentarySound;
	public AudioClip tickTuckSound;
	public AudioClip questionBell;
	public AudioClip choiceBell;

	// kind of weird
	AudioSource audiosrc;
	AudioSource bgm;

	// public sprites
	public Sprite nextBtnSprite;
	public Sprite answerBtnSprite;

	// for Ruby Components, need to have several Vector3 variables
	public Vector3 choicesRubyComponentVec3;
	public GameObject choicesTextComponent;
	public GameObject questionTextComponent;
	public GameObject commentaryTextComponent;
	public Transform canvasInScene;
	public Transform choicesTextTransform;
	public Transform questionTextTransform;
	public Transform commentaryTextTransform;

	// ping pong colors for fade in/out animation
	//private Color 

	public class QuizObj
	{
		public int id;
		public string questionText;
		public string answer;
		public string choiceA;
		public string choiceB;
		public string choiceC;
		public string commentary;
		public int order;
		public int packageId;
		public string mode;

		// Constructor
		public QuizObj(int id, string questionText, string answer, string choiceA, string choiceB, string choiceC, string commentary, int order, int packageId, string mode )
		{
			this.id = id;
			this.questionText = questionText;
			this.answer = answer;
			this.choiceA = choiceA;
			this.choiceB = choiceB;
			this.choiceC = choiceC;
			this.commentary = commentary;
			this.order = order;
			this.packageId = packageId;
			this.mode = mode;
		}
	}
	
	
	// Use this for initialization
	void Start () {
		// somehow need to get component in script
		//audiosrc = GetComponent<AudioSource>();
		bgm = GetComponents<AudioSource>()[1];
		audiosrc = GetComponents<AudioSource>()[0];
		Debug.Log (audiosrc.clip.name);

		// anim disable
		anim.enabled = false;
		// set next button
		nextBtn = GameObject.Find("Next Button");
		transparentWhite = new Color (1.0f, 1.0f, 1.0f, 0.0f);
		// GET request
		//sendGetRequest ();

		// set up the reference of "carObj" variable
		carObj = GameObject.Find(carComponentName);
		isInstructionReady = true;
	}
	
	// Update is called once per frame
	void Update () {
		// check where I am
		int stateTimerNumber = checkCurrentState();
		// and manage timer
		manageTimer(stateTimerNumber);
		// instruction manager
		if(isInstructionReady) {
			StartCoroutine("showInstruction");
		}
		if(isQuestionTextReady) {
			StartCoroutine (showQuestionText ());
		}
		if(isChoicesTextReady){
			StartCoroutine ("showChoicesText");
		}
		if(isAnswerReady){
			StartCoroutine (showAnswer());
		}
		if(isCommentaryTextReady){
			StartCoroutine ("showCommentary");
		}

		// keydown thing
		checkKeydown();

		// when the car is changing the line
		if(
			(isCarChangingLaneToLeftFromCenter) && 
			(!isCarChangingLaneToCenterFromLeft) && 
			(!isCarChangingLaneToCenterFromRight) && 
			(!isCarChangingLaneToRightFromCenter)
		) 
		{
			// animate winker or not?
			if (isInitialAnimationCall) {
				StartCoroutine (winkWinkWinkLeft ());
				// change boolean state as well
				isInitialAnimationCall = false;
			}
			//StartCoroutine (changeLane ());
			changeLaneToLeftFromCenter();
		}

		else if(
			(!isCarChangingLaneToLeftFromCenter) && 
			(isCarChangingLaneToCenterFromLeft) && 
			(!isCarChangingLaneToCenterFromRight) && 
			(!isCarChangingLaneToRightFromCenter)	
		)
		{
			// animate winker or not?
			if (isInitialAnimationCall) {
				StartCoroutine (winkWinkWinkRight ());
				// change boolean state as well
				isInitialAnimationCall = false;
			}
			changeLaneToCenterFromLeft ();
		}

		else if(
			(!isCarChangingLaneToLeftFromCenter) && 
			(!isCarChangingLaneToCenterFromLeft) && 
			(isCarChangingLaneToCenterFromRight) && 
			(!isCarChangingLaneToRightFromCenter)	
		)
		{
			// animate winker or not?
			if (isInitialAnimationCall) {
				StartCoroutine (winkWinkWinkLeft ());
				// change boolean state as well
				isInitialAnimationCall = false;
			}
			changeLaneToCenterFromRight ();
		}
		else if(
			(!isCarChangingLaneToLeftFromCenter) && 
			(!isCarChangingLaneToCenterFromLeft) && 
			(!isCarChangingLaneToCenterFromRight) && 
			(isCarChangingLaneToRightFromCenter)	
		)
		{
			// animate winker or not?
			if (isInitialAnimationCall) {
				StartCoroutine (winkWinkWinkRight ());
				// change boolean state as well
				isInitialAnimationCall = false;
			}
			changeLaneToRightFromCenter();
		}

		if (
			(!isCarChangingLaneToLeftFromCenter) && 
			(!isCarChangingLaneToCenterFromLeft) && 
			(!isCarChangingLaneToCenterFromRight) && 
			(!isCarChangingLaneToRightFromCenter)
		) {
			// always check acceleration
			checkAccelerationThreshold();

			// and fix car y axis, if it is tilted..
			fixTilt();
		}

		// too many if, finally we check if the timer animation is on or not
		if (isTimerAnimationOn) {
			animateTimer ();
		}

		// check quiz start btn is fading..
		if (isQuizStartBtnFading) {
			fadeBtn(GameObject.Find("Quiz Start Button"));
		}

		if (isQuestionNumFadingIn) {
			fadeInQuestionNumber (buttons [currentQuizIndex]);
		} else if (isQuestionNumFadingOut) {
			fadeOutQuestionNumber (buttons [currentQuizIndex]);
		}
	}

	IEnumerator waitThenDestroy(GameObject target){
		yield return new WaitForSeconds (1.0f);
		Destroy (target);
	}

	void fadeBtn(GameObject targetObj){
		targetObj.GetComponent<Image>().color = Color.Lerp(Color.white, transparentWhite, Time.fixedTime - (fadeStartTime - 0.5f));
		//targetObj.GetComponent<Image>().color = Color.Lerp(Color.white, transparentWhite, Time.time);
		if (targetObj.GetComponent<Image>().color.a <= 0.0f) {
			isQuizStartBtnFading = false;
		}
	}

	void checkKeydown(){
		// temporarily add key press event
		if (Input.GetKeyDown("r")) {
			// play audio clip as a key down event
			audiosrc.PlayOneShot(commentarySound, 1.0f);
			anim.enabled = true;
			Debug.Log ("key pressed. the value is r.");
			//isCarChangingLaneToLeftFromCenter = true;
			StartCoroutine (winkWinkWinkRight ());
			//StopCoroutine(showQuestionText());
			StopCoroutine("showInstruction");
			// after 5seconds, remove instruction text.
			gameObject.transform.Find("InstructionText").transform.localScale = new Vector3(0, 0, 0);
			gameObject.transform.Find("ObjA").transform.localScale = new Vector3(0, 0, 0);
			gameObject.transform.Find("ObjB").transform.localScale = new Vector3(0, 0, 0);
			gameObject.transform.Find("ObjC").transform.localScale = new Vector3(0, 0, 0);
			Debug.Log ("now instruction text gets invisible..");
			// and swap bool of question text visibility..
			isQuestionTextReady = true;
		}

		if (Input.GetKeyDown("l")) {
			Debug.Log ("key pressed. the value is l.");
			// test stop coroutine
			StopCoroutine(showInstruction());
		}

		if (Input.GetKeyDown("o")) {
			Debug.Log ("key pressed. the value is o.");
			// test stop coroutine
			anim.SetInteger("Animation Mode", 0);
			StartCoroutine (AudioFadeOut.FadeOut (bgm, 1.5f));
		}
	}

	/*
	 * How many "IEnumerators" I made...
	 * Definitely possible to code less and less
	 * he said, "Do not repeat yorself.."
	 */
	void animateTimer(){
		// dist
		float dist = Time.fixedTime - timerStartTime;
		float eulerZ = -24 * dist;
		// now got the eulerx, then rotate secondhand
		GameObject.Find("timer_hand").transform.eulerAngles = new Vector3(0, 0, eulerZ);
	}

	IEnumerator showInstruction() {
		// reset component scale
		gameObject.transform.Find("InstructionText").transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
		// first, swap bool
		isInstructionReady = false;
		// wait!
		yield return new WaitForSeconds (20.0f);
		hideInstructionStuff ();
	}

	// method to skip instruction time..
	public void SkipInstruction(){
		StopCoroutine("showInstruction");
		hideInstructionStuff ();
	}

	void hideInstructionStuff(){
		// get time!
		fadeStartTime = Time.fixedTime;
		// remove event listener
		GameObject.Find("Quiz Start Button").GetComponent<Button>().onClick.RemoveAllListeners();
		Debug.Log ("once is enough....");

		gameObject.transform.Find("InstructionText").transform.localScale = new Vector3(0, 0, 0);
		gameObject.transform.Find("ObjA").transform.localScale = new Vector3(0, 0, 0);
		gameObject.transform.Find("ObjB").transform.localScale = new Vector3(0, 0, 0);
		gameObject.transform.Find("ObjC").transform.localScale = new Vector3(0, 0, 0);
		Debug.Log ("now instruction text gets invisible..");
		// and swap bool of question text visibility..
		//isQuestionTextReady = true;
		// may be like this
		StartCoroutine(waitThenChangeQuestionReasy());
		// remove button finally
		//Destroy(GameObject.Find("Quiz Start Button"));
		isQuizStartBtnFading = true;
		StartCoroutine (waitThenDestroy (GameObject.Find("Quiz Start Button")));
		// enable anim
		anim.enabled = true;
	}

	IEnumerator waitThenChangeQuestionReasy(){
		yield return new WaitForSeconds (1.0f);
		isQuestionTextReady = true;
	}

	IEnumerator showQuestionText(){
		// play question bell
		playQuestionBell();
		// get time stamp here.
		FadeStartTimeOfQuestionNum = Time.fixedTime;
		// we want to show question number form currentQuizIndex..
		isQuestionNumFadingIn = true;
		// boefore make the component visible, need to set the text correctly.
		string outText = getQuestionText(currentQuizIndex);
		// and set the string to the component
		setQuestionText(outText);
		// also, we wanto to set ruby text for the question text
		//setRubyForQuestionText(outText);
		setRubyComponentManagerForQuestionText(outText);
		// reset component scale
		gameObject.transform.Find("QuestionText").transform.localScale = new Vector3(1, 1, 1);
		// first, swap bool
		isQuestionTextReady = false;
		// wait!
		yield return new WaitForSeconds(7.0f);
		// after 5seconds, need to show choices as well.
		Debug.Log("now we want to show choices A, B, C respectively..");
		// change boolean..
		isChoicesTextReady = true;
	}

	void setRubyForQuestionText(string outputText) {
		// get how many "ruby" tags in the string
		//int length = 
	}

	// fadeInQuestionber
	void fadeInQuestionNumber(GameObject targetObj) {
		targetObj.GetComponent<Image> ().color = Color.Lerp (transparentWhite, Color.white, Time.fixedTime - (FadeStartTimeOfQuestionNum - 0.1f));
		if (targetObj.GetComponent<Image>().color.a >= 1.0f) {
			isQuestionNumFadingIn = false;
		}
	}

	// fadeOutQuestionNumber
	void fadeOutQuestionNumber(GameObject targetObj){
		targetObj.GetComponent<Image> ().color = Color.Lerp (Color.white, transparentWhite, Time.fixedTime - (FadeStartTimeOfQuestionNum - 0.5f));
		if (targetObj.GetComponent<Image>().color.a <= transparentWhite.a) {
			isQuestionNumFadingOut = false;
		}
	}

	IEnumerator showChoicesText(){
		// change sprite here
		nextBtn.GetComponent<Image>().sprite = answerBtnSprite;
		// play choice sound effects
		playChoiceSoundEffects();

		// this time, need to add event lister in script
		nextBtn.GetComponent<Button>().onClick.AddListener(() => { checkAnswerInstantly(); });
		// start fade in as well
		NextButtonViewController.fadeStartTime = Time.fixedTime;
		NextButtonViewController.isNextBtnFadingIn = true;
		// boefore make the component visible, need to set the text correctly.
		string outText = getChoices(currentQuizIndex);
		// ruby thing
		setRubyComponentManager (outText);
		// and set the string to the component
		setChoices(outText);
		// reset component scale
		GameObject.Find("ChoicesText").transform.localScale = new Vector3(1, 1, 1);
		// first swap bool
		isChoicesTextReady = false;
		// and we want to show A,B,C in the scene for the user to understand which position in the scene means the choice.
		SwapChoicesVisibilityInScene(true);

		// start timer
		GameObject.Find("Timer").transform.localScale = new Vector3(1, 1, 1);
		GameObject.Find("Timer").transform.eulerAngles = new Vector3(0, 0, 0);
		timerStartTime = Time.fixedTime;
		triggerTimerAnimation();

		// bit wait !
		yield return new WaitForSeconds(0.4f);
		playTickTuck ();

		// wait!
		yield return new WaitForSeconds(14.6f);
		hideChoiceStuff ();
	}

	public void checkAnswerInstantly(){
		StopCoroutine ("showChoicesText");
		hideChoiceStuff ();
	}

	void hideChoiceStuff(){
		// next button thing
		NextButtonViewController.fadeStartTime = Time.fixedTime;
		NextButtonViewController.isNextBtnFadingOut = true;
		// remove event listener
		nextBtn.GetComponent<Button>().onClick.RemoveAllListeners();


		// now stop timer
		isTimerAnimationOn = false;
		// hide timer
		GameObject.Find("Timer").transform.localScale = new Vector3(0, 0, 0);
		// after 5seconds, need to check the car position, which uses to determine the selected answer.
		Debug.Log("now timer ends up, let's check where the car is.");
		// user now wair for the correct answer, so just make unvisible A, B, C in the scene
		SwapChoicesVisibilityInScene(false);

		// now, we want to show the actual answer.
		isAnswerReady = true;
		// and want to show answer text only..
		GameObject.Find("ChoicesText").transform.localScale = new Vector3(0, 0, 0);
		GameObject.Find ("QuestionText").transform.localScale = new Vector3 (0, 0, 0);
		// and destroy choices text components containing ruby.
		destroyRubyComponentsInSelectedObj ("ChoicesText");
		// destroy ruby of question as well
		destroyRubyComponentsInSelectedObj("QuestionText");
	}

	IEnumerator showAnswer(){
		// output
		string outputText;
		// answer by user
		string userChoice = sendFeedback ();
		// correct answer
		string correctAnswer = quizObjs[currentQuizIndex].answer;
		// stop tick tuck
		audiosrc.Stop();
		//check if
		if (userChoice == correctAnswer || correctAnswer == "ABC") {
			// play ok SE
			playOKSoundEffects();

			outputText = "おめでとう！正解は" + correctAnswer + "だよ！";
			// we also want to add score 10 by one
			survive.score += 20;
			// ok text image
			GameObject.Find("text_ok").transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
			// ok character image
			GameObject.Find("character_ok").transform.localScale = new Vector3(1, 1, 1);
		} else {
			// play ng SE
			playNGSoundEffects();

			outputText = "ざんねん！正解は" + correctAnswer + "だよ！";
			// ok text image
			GameObject.Find("text_no").transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
			// ok character image
			GameObject.Find("character_no").transform.localScale = new Vector3(1, 1, 1);
		}

		// change view of the text component as well. need to chunk lines below..
		Text[] texts = gameObject.GetComponentsInChildren<Text> ();
		foreach (Text text in texts) {
			if(text.name == "Answer") {
				text.text = outputText;
			}
		}

		// reset answer component scale
		GameObject.Find("Answer").transform.localScale = new Vector3(1, 1, 1);


		// first swap bool
		isAnswerReady = false;
		// wait! this time, 2seconds might be enough.
		yield return new WaitForSeconds(2.0f);
		// after 2 seconds, need to show the corresponding commentary.
		Debug.Log("some animation and sound effects(two types, true or false) ends, now we want to see the commentary");
		// now, we want to show the comment.
		isCommentaryTextReady = true;
	}

	string sendFeedback(){
		string selectedAnswer;
		// where is the car?
		float carXPos = GameObject.Find(carComponentName).transform.position.x;
		// from the position, we could determine the selected choice.
		if (carXPos < -1) {
			// car is at the left side
			selectedAnswer = "A";
		} else if (carXPos < 1) {
			// car is at the center
			selectedAnswer = "B";
		} else {
			// car is at the right side
			selectedAnswer = "C";
		}
		return selectedAnswer;
	}

	public void skipCommentary(){
		StopCoroutine ("showCommentary");
		StartCoroutine(hideCommentaryStuff ());
	}

	public IEnumerator hideCommentaryStuff(){
		nextBtn.GetComponent<Button>().onClick.RemoveAllListeners();
		// get time stamp here.
		FadeStartTimeOfQuestionNum = Time.fixedTime;
		// and fade out..
		isQuestionNumFadingOut = true;

		// next button thing
		NextButtonViewController.fadeStartTime = Time.fixedTime;
		NextButtonViewController.isNextBtnFadingOut = true;

		// might be better to wait for another 3 seconds?
		yield return new WaitForSeconds(2.0f);

		// after 5seconds, need to move to the next question, to do that, also need to check the index of the last question.
		Debug.Log("now commentary is going to fade out, what should we do next? how do we know this?");
		// before that, at least we need to make those components invisible.
		gameObject.transform.Find("CommentaryText").transform.localScale = new Vector3(0, 0, 0);
		gameObject.transform.Find ("Answer").transform.localScale = new Vector3 (0, 0, 0);
		// destroy children in commentary text..
		destroyRubyComponentsInSelectedObj ("CommentaryText");

		// hide hide hide
		// ok text image
		GameObject.Find("text_ok").transform.localScale = new Vector3(0, 0, 0);
		// ok character image
		GameObject.Find("character_ok").transform.localScale = new Vector3(0, 0, 0);
		// no text image
		GameObject.Find("text_no").transform.localScale = new Vector3(0, 0, 0);
		// no character image
		GameObject.Find("character_no").transform.localScale = new Vector3(0, 0, 0);

		// how about this? need to establish condition, but might work like a infinite loop.
		Debug.Log(currentQuizIndex);
		if (currentQuizIndex == 4) {
			//Application.
			//SceneManager.LoadScene (3);
			StartCoroutine (AudioFadeOut.FadeOut (bgm, 1.5f));
			StartCoroutine(waitThenScoreScene());

		} else {
			isQuestionTextReady = true;
			// and increment value
			incrementCurrentQuizIndex();
		}
	}

	IEnumerator showCommentary(){
		// we want to change sprite.
		nextBtn.GetComponent<Image>().sprite = nextBtnSprite;
		// this time, need to add event lister in script
		nextBtn.GetComponent<Button>().onClick.RemoveAllListeners();
		nextBtn.GetComponent<Button>().onClick.AddListener(() => { skipCommentary(); });
		// start fade in as well
		NextButtonViewController.fadeStartTime = Time.fixedTime;
		NextButtonViewController.isNextBtnFadingIn = true;

		// play commentary sound effects
		playCommentarySoundEffects();
		string outputText = quizObjs[currentQuizIndex].commentary;
		// change view of the text component as well. need to chunk lines below..
		Text[] texts = gameObject.GetComponentsInChildren<Text> ();
		foreach (Text text in texts) {
			if(text.name == "CommentaryText") {
				text.text = getRichTextForMainComponent(outputText);
			}
		}
		// and set ruby for commentary
		setRubyComponentManagerForCommentaryText(outputText);
		// reset commentary component scale
		gameObject.transform.Find("CommentaryText").transform.localScale = new Vector3(1, 1, 1);
		// first swap bool
		isCommentaryTextReady = false;
		// wait! might take 5seconds..
		yield return new WaitForSeconds(13.0f);
		StartCoroutine (hideCommentaryStuff ());
	}

	IEnumerator waitThenScoreScene(){
		yield return new WaitForSeconds (1.5f);
		SceneManager.LoadScene (3);
	}

	int checkCurrentState(){
		int stateNumber = 0;
		return stateNumber;
	}

	void manageTimer(int stateTimerNumber){
		float defaultTime = 30.0f;
		defaultTime -= Time.deltaTime;
	}

	/*
	 * www thing
	 * methods to handle with data
	 */
	void sendGetRequest(){
		WWWForm form = new WWWForm ();
		form.AddField ("Username", "naked");
		form.AddField ("Password", "57905191");

		var headers = form.headers;
		//var rawData = form.data;
		string secretKey = "naked" + ":" + "57905191";
		headers["Authorization"]="Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes(secretKey));

		// the code snippet below is the same with one on the unity reference web page, but never works. how weird.
		//headers["Authorization"] = "Basic " + System.Convert.ToBase64String(
		//System.Text.Encoding.ASCII.GetBytes("username:password"));

		// in case request is GET not POST, the second parameter could be null.
		WWW www = new WWW(url, null, headers);
		//yield
		StartCoroutine(WaitForRequest(www));
	}

	IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		// check for errors
		if (www.error == null)
		{
			Debug.Log("WWW Ok!: " + www.text);

			/*
			 * put this method here is weird.
			 */
			// now we've got enough data so as to be ready for the instruction..
			isInstructionReady = true;
			/*
			 * put this method here is weird.
			 */

		} else {
			Debug.Log("WWW Error: "+ www.error);
		} 
	}

	// regular expresion
	string removeDoubleQuotes(string targetString) {
		// replace double quote with nothing, which is equal to removing double quotes..
		string outStr = targetString.Replace ("\"", "");
		return outStr;
	}

	// decode multibyte characters
	string decodeChars(string originalStr) {
		string outStr = System.Text.RegularExpressions.Regex.Unescape (originalStr);
		return outStr;
	}
		

	/*
	 * access car
	 * get position
	 */

	void checkAccelerationThreshold(){
		// we want to check the value of gyro. if user tilts the device really quickly, the car move to another line.
		// to do this, maybe it works when the input range(0~1) is higher than 0.8. for instance.
		float accelerationVal = Input.acceleration.x;
		//float currentPosX = GameObject.Find (carComponentName).transform.position.x;
		float currentPosX = carObj.transform.position.x;
		// there are 4 types to move the car, left from center, right from cetner, left from the right side, right from the left side.

		if (accelerationVal > 0.2) {
			Debug.Log ("passed threshold!");
			// also, animation is not going on
			if (currentPosX < -1.0f) {
				isCarChangingLaneToCenterFromLeft = true;
			} else {
				isCarChangingLaneToRightFromCenter = true;
			}

		} else if (accelerationVal < -0.2) {
			Debug.Log ("yes you tilted the device to the right, yikes");
			//start
			// another condition
			if (currentPosX < 1.0f) {
				isCarChangingLaneToLeftFromCenter = true;
			} else {
				isCarChangingLaneToCenterFromRight = true;
			}
		}
	}

	// test method, move left!
	void changeLaneToLeftFromCenter(){
		float currentPosX = GameObject.Find (carComponentName).transform.position.x;
		float result = currentPosX - 0.05f;

		if (currentPosX > -1.8f) {
			GameObject.Find (carComponentName).transform.position = new Vector3(result, 0, -1.3f);

			// nesting is not ideal implementation.
			if (currentPosX > -0.9f) {
				GyroController.rotateFrontTiresOnTheYAxisAntiClockwise ();
			} else {
				//GyroController.rotateFrontTiresOnTheYAxisClockwise ();
			}
		} else {
			// no animation anymore..
			isCarChangingLaneToLeftFromCenter = false;
			// winker animation ends as well, set ready to be called
			isInitialAnimationCall = true;
		}
	}

	void changeLaneToRightFromCenter() {
		// set winker
		//StartCoroutine(winkWinkWinkRight());
		float currentPosX = GameObject.Find (carComponentName).transform.position.x;
		float result = currentPosX + 0.05f;
		if (currentPosX < 1.8f) {
			// till get the edge, move the car..
			GameObject.Find (carComponentName).transform.position = new Vector3 (result, 0, -1.3f);
			GyroController.rotateFrontTiresOnTheYAxisAntiClockwise ();
		} else {
			isCarChangingLaneToRightFromCenter = false;
			// winker animation ends as well, set ready to be called
			isInitialAnimationCall = true;
		}
	}

	void changeLaneToCenterFromLeft(){
		float currentPosX = GameObject.Find (carComponentName).transform.position.x;
		float result = currentPosX + 0.05f;
		if (currentPosX < 0) {
			GameObject.Find (carComponentName).transform.position = new Vector3 (result, 0, -1.3f);
			GyroController.rotateFrontTiresOnTheYAxisClockwise ();
		} else {
			isCarChangingLaneToCenterFromLeft = false;
			// winker animation ends as well, set ready to be called
			isInitialAnimationCall = true;
		}
	}

	void changeLaneToCenterFromRight(){
		float currentPosX = GameObject.Find (carComponentName).transform.position.x;
		float result = currentPosX - 0.05f;
		if (currentPosX > 0) {
			GameObject.Find (carComponentName).transform.position = new Vector3 (result, 0, -1.3f);
			// nesting is not ideal implementation.
			if (currentPosX > 0.9f) {
				GyroController.rotateFrontTiresOnTheYAxisAntiClockwise ();
			}
		} else {
			isCarChangingLaneToCenterFromRight = false;
			// winker animation ends as well, set ready to be called
			isInitialAnimationCall = true;
		}	
	}


	/*
	 * quiz thing
	 * 
	 */
	void SwapChoicesVisibilityInScene(bool isVisible) {
		if (isVisible) {
			GameObject.Find("ObjA").transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
			GameObject.Find("ObjB").transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
			GameObject.Find("ObjC").transform.localScale = new Vector3(0.9f, 0.9f, 0.9f);
		} else {
			GameObject.Find("ObjA").transform.localScale = new Vector3(0, 0, 0);
			GameObject.Find("ObjB").transform.localScale = new Vector3(0, 0, 0);
			GameObject.Find("ObjC").transform.localScale = new Vector3(0, 0, 0);
		}
	}

	void setQuestionText(string outputText){
		Text[] texts = gameObject.GetComponentsInChildren<Text> ();
		foreach(Text text in texts){
			if(text.name == "QuestionText"){
				text.text = getRichTextForMainComponent(outputText);
			} else if (text.name == "QuestionTextRuby") {
				// if ruby, need to get "ruby"-style rich text
				//text.text = outputText;
			}
		}
	}

	string getQuestionText(int index){
		return quizObjs [index].questionText;
	}

	string getAnswer(int index){
		return quizObjs [index].answer;
	}

	string getChoices(int index){
		QuizObj tmp = quizObjs [index];
		string textOfChunkedChoices = "A：" + tmp.choiceA + System.Environment.NewLine + "B：" + tmp.choiceB + System.Environment.NewLine + "C：" + tmp.choiceC;
		return textOfChunkedChoices;
	}

	void setChoices(string outputText){
		Text[] texts = gameObject.GetComponentsInChildren<Text> ();
		foreach(Text text in texts) {
			if(text.name == "ChoicesText") {
				//text.text = outputText;
				text.text = getRichTextForMainComponent (outputText);
			} else if (text.name == "ChoicesTextRuby") {
				// if ruby, need to get "ruby"-style rich text
				text.text = outputText;
			}
		}
	}

	void setRubyComponentManager(string outputText) {
		// fitst, get how many ruby tags in the string
		Regex regex = new Regex("<ruby.+?/ruby>");
		int length = regex.Matches (outputText).Count;
		string[] fixedArray = new string[length];
		fixedArray = getRichTextForNthRuby (outputText, length);
		Debug.Log (length);
		Debug.Log (outputText);
		// going to call for loop
		for (int i = 0; i < length; i++) {
			GameObject temp = Instantiate (choicesTextComponent, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			temp.transform.parent = choicesTextTransform;
			temp.transform.localPosition = new Vector3 (0, 45, 0);
			temp.transform.localScale = new Vector3 (1, 1, 1);
			temp.transform.localRotation = new Quaternion (0, 0, 0, 0);
			int tempVal = i;
			temp.GetComponent<Text>().text = fixedArray[tempVal];
			temp.GetComponent<Text> ().SetAllDirty ();
		}
	}

	void setRubyComponentManagerForQuestionText(string outputText) {
		// fitst, get how many ruby tags in the string
		Regex regex = new Regex("<ruby.+?/ruby>");
		int length = regex.Matches (outputText).Count;
		string[] fixedArray = new string[length];
		fixedArray = getRichTextForNthRuby (outputText, length);
		Debug.Log (length);
		Debug.Log (outputText);
		// going to call for loop
		for (int i = 0; i < length; i++) {
			GameObject temp = Instantiate (questionTextComponent, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			temp.transform.parent = questionTextTransform;
			//temp.transform.localPosition = new Vector3 (0, 55, 0);
			temp.transform.localPosition = new Vector3 (-774, 55, 0);
			temp.transform.localScale = new Vector3 (1, 1, 1);
			temp.transform.localRotation = new Quaternion (0, 0, 0, 0);
			int tempVal = i;
			temp.GetComponent<Text>().text = fixedArray[tempVal];
			temp.GetComponent<Text> ().SetAllDirty ();
		}
	}

	void setRubyComponentManagerForCommentaryText(string outputText) {
		// fitst, get how many ruby tags in the string
		Regex regex = new Regex("<ruby.+?/ruby>");
		int length = regex.Matches (outputText).Count;
		string[] fixedArray = new string[length];
		fixedArray = getRichTextForNthRuby (outputText, length);
		Debug.Log (length);
		Debug.Log (outputText);
		// going to call for loop
		for (int i = 0; i < length; i++) {
			GameObject temp = Instantiate (commentaryTextComponent, new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			temp.transform.parent = commentaryTextTransform;
			//temp.transform.localPosition = new Vector3 (0, 45, 0);
			temp.transform.localPosition = new Vector3 (-768, 45, 0);
			temp.transform.localScale = new Vector3 (1, 1, 1);
			temp.transform.localRotation = new Quaternion (0, 0, 0, 0);
			int tempVal = i;
			temp.GetComponent<Text>().text = fixedArray[tempVal];
			temp.GetComponent<Text> ().SetAllDirty ();
		}
	}

	void destroyRubyComponentsInSelectedObj(string targetObj){
		Text[] textArr = GameObject.Find(targetObj).GetComponentsInChildren<Text> ();
		foreach (Text text in textArr) {
			if (text.name != targetObj) {
				Destroy (text.gameObject);		
			}
		}
	}

	// method like this surely affect the stack of the program.. might need to remove global variable.
	void incrementCurrentQuizIndex() {
		// increment!
		currentQuizIndex++;
	}

	// winker
	void winkLeft(){
		GameObject.Find("tail_lamp_L").GetComponent<MeshRenderer>().material = EmissionMat;
	}

	void winkLeftFromRed(){
		GameObject.Find ("tail_lamp_L").GetComponent<MeshRenderer> ().material = lampLeftMat;
	}

	void winkRight(){
		// the following two lines of code does not work on tablet!
		//GameObject.Find("tail_lamp_R").GetComponent<MeshRenderer>().material.EnableKeyword("_EMISSION");
		GameObject.Find ("tail_lamp_R").GetComponent<MeshRenderer> ().material = EmissionMat;
	}

	void winkRightFromRed(){
		GameObject.Find ("tail_lamp_R").GetComponent<MeshRenderer> ().material = lampRightMat;
	}

	IEnumerator winkWinkWinkRight(){
		// wait!
		yield return new WaitForSeconds(0.5f);
		// wink
		winkRight();
		// wait!
		yield return new WaitForSeconds(0.5f);
		// blink
		winkRightFromRed();
		// wait!
		yield return new WaitForSeconds(0.5f);
		// wink
		winkRight();
		// wait!
		yield return new WaitForSeconds(0.5f);
		// blink
		winkRightFromRed();
	}

	IEnumerator winkWinkWinkLeft(){
		// wait!
		yield return new WaitForSeconds(0.5f);
		// wink
		winkLeft();
		// wait!
		yield return new WaitForSeconds(0.5f);
		// blink
		winkLeftFromRed();
		// wait!
		yield return new WaitForSeconds(0.5f);
		// wink
		winkLeft();
		// wait!
		yield return new WaitForSeconds(0.5f);
		// blink
		winkLeftFromRed();
	}

	IEnumerator triggerYAxisRotation(){
		GyroController.rotateFrontTiresOnTheYAxisClockwise ();

		yield return new WaitForSeconds (0.8f);

		GyroController.rotateFrontTiresOnTheYAxisAntiClockwise ();
	}

	void fixTilt(){
		// first, need to check the y axis rotation..
		float yTiltValEuler = GameObject.Find("NEXCO_carmodel_0206_01").transform.rotation.eulerAngles.y;
		// we want to keep the default y rotation value between -0.05 ~ 0.05 (possibly)
		if (yTiltValEuler > 0.5 && yTiltValEuler < 180) {
			// it it's bigger, we want to slightly decrease the y value as close as 0
			GameObject.Find ("NEXCO_carmodel_0206_01").transform.eulerAngles = new Vector3 (0, yTiltValEuler - 0.2f, 0);
		} else if (yTiltValEuler < 359.5) {
			GameObject.Find ("NEXCO_carmodel_0206_01").transform.eulerAngles = new Vector3 (0, yTiltValEuler + 0.2f, 0);
		}
	}

	void triggerTimerAnimation(){
		isTimerAnimationOn = true;
	}

	// first, we want to set renderqueue of the transparent object
	void setRenderQueue(){
		// first set it 1000..
		GameObject.Find ("tail_lamp_L").GetComponent<MeshRenderer> ().material.renderQueue = 0;
		GameObject.Find ("tail_lamp_R").GetComponent<MeshRenderer> ().material.renderQueue = 1;
	}


	/*
	 * sound stuff goes on..
	 * really not sure those methods are maintanable..
	 */

	public void playCommentarySoundEffects(){
		//commentarySound.
		audiosrc.PlayOneShot(commentarySound, 1.0f);
	}

	public void playOKSoundEffects(){
		audiosrc.PlayOneShot (okSound, 1.0f);
	}

	public void playNGSoundEffects(){
		audiosrc.PlayOneShot (ngSound, 1.0f);
	}

	public void playQuestionBell(){
		audiosrc.PlayOneShot (questionBell, 1.0f);
	}

	public void playTickTuck(){
		audiosrc.PlayOneShot (tickTuckSound, 0.6f);
	}

	public void playChoiceSoundEffects(){
		audiosrc.PlayOneShot (choiceBell, 1.4f);
	}



	/*
	 * Regular Expression Methods.
	 */ 
	string getRichTextForMainComponent(string originalText){
		string replacement = "";
		string preResultStr = Regex.Replace (originalText, "<ruby.+?'>", replacement);
		string resultStr = Regex.Replace (preResultStr, "</ruby>", replacement);
		return resultStr;
	}

	string getRichTextForRubyComponent(string originalText){
		string replacementA = "<color=#ffffffff><ruby='";
		string replacementB = "</ruby></color>";
		string replacementC = "<color=#ffffffff>";
		string replacementD = "</color>";
		string preResultStr = Regex.Replace (originalText, "<ruby='", replacementA);
		string resultStr = Regex.Replace (preResultStr, "</ruby>", replacementB);
		resultStr = Regex.Replace (resultStr, "<ruby='", replacementC);
		resultStr = Regex.Replace (resultStr, "'>", replacementD);
		resultStr = Regex.Replace(resultStr, "</ruby>", "");
		return resultStr;
	}

	string[] getRichTextForNthRuby(string originalText, int index){
		Regex regex = new Regex(@"<ruby(.+?)/ruby>");
		Regex regexB = new Regex (@"\'>(.+?)/ruby>");
		int length = regex.Matches (originalText).Count;
		//int length = 7;
		string[] rubies = new string[length];
		string[] fixedStr = new string[length];
		int foreachCounter = 0;
		foreach (Match match in regex.Matches(originalText)) {
			rubies [foreachCounter] = match.Value;
			string onlyRuby = rubies [foreachCounter].Replace ("<ruby=\'", "");
			//onlyRuby = onlyRuby.Replace (regexB, "");
			onlyRuby = regexB.Replace (onlyRuby, "");

			// another replace..
			onlyRuby = onlyRuby.Replace("_", "<color=#00000000>a</color>");

			string rubyPlusSizeTag = "<color=#ffffffff><size=26>" + onlyRuby + "</size></color>";
			//string rubyPlusSizeTag = "<color=#ffffffff>" + onlyRuby + "</color>";
			fixedStr [foreachCounter] = originalText.Replace (rubies [foreachCounter], rubyPlusSizeTag);
			fixedStr [foreachCounter] = getRichTextForMainComponent (fixedStr [foreachCounter]);
			foreachCounter++;
			Debug.Log (foreachCounter);
		}

		// finally we need to wipe out redundant <ruby> tags..
		//string returnedStr = getRichTextForMainComponent(fixedStr[index]);

		return fixedStr;
	}
}
