﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameSettingManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// set UI click events
	public void ChangeView(){
		//Application.LoadLevel ("select-scene");
		SceneManager.LoadScene (1);
		//Application.LoadScene("select-scene");

		//Debug.Log("hi there:)");
	}

	public IEnumerator waitAndGoSelectScene(){
		yield return new WaitForSeconds (1.5f);
		SceneManager.LoadScene (1);
	}

	public void WaitAndChangeView(){
		StartCoroutine (waitAndGoSelectScene ());
	}

	public void ChangeScene(){
		//Application.LoadLevel ("nexco-game-scene");
		//SceneManager.LoadScene(2);
		StartCoroutine(waitAndGoToGameScene());
	}

	public IEnumerator waitAndGoToGameScene(){
		yield return new WaitForSeconds (1.0f);
		SceneManager.LoadScene (2);
	}

	public void GoStartScene(){
		// reset game score..
		survive.score = 0;
		// reset selected level
		survive.selectedLevel = 0;
		SceneManager.LoadScene (0);
	}
}
