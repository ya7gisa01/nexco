﻿using UnityEngine;
using System.Collections;
// using UI as well
using UnityEngine.UI;

public class NextButtonViewController : MonoBehaviour {

	// what we need is a bool type variable.
	public static bool isNextBtnFadingIn = false;
	public static bool isNextBtnFadingOut = false;

	// fadeStarttime use static modifier
	public static float fadeStartTime;

	private GameObject nextBtn;
	private Color transparentWhite = new Color(1.0f, 1.0f, 1.0f, 0.0f);

	// Use this for initialization
	void Start () {
		// assign value first
		nextBtn = GameObject.Find("Next Button");
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isNextBtnFadingIn) {
			FadeInNextBtn (nextBtn);
		} else if (isNextBtnFadingOut) {
			FadeOutNextBtn (nextBtn);
		}
		
	}

	void FadeInNextBtn(GameObject targetObj){
		targetObj.GetComponent<Image>().color = Color.Lerp(transparentWhite, Color.white, Time.fixedTime - (fadeStartTime - 0.5f));
		if (targetObj.GetComponent<Image>().color.a >= 1.0f) {
			isNextBtnFadingIn = false;
		}
	}

	void FadeOutNextBtn(GameObject targetObj){
		targetObj.GetComponent<Image>().color = Color.Lerp(Color.white, transparentWhite, Time.fixedTime - (fadeStartTime - 0.5f));
		if (targetObj.GetComponent<Image>().color.a <= transparentWhite.a) {
			isNextBtnFadingOut = false;
		}
	}
}
