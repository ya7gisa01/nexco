﻿using UnityEngine;
using System.Collections;

public class GyroController : MonoBehaviour {
	// gui style
	public GUIStyle guistyle;

	// variables for timers
	float instructionTime = 10.0f;
	bool isInstructionTime = true;
	bool isInQuiz = false;
	float tireCounter = 0.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//transform.Translate (Input.acceleration.x, 0, 0);
		checkCarPos ();

		// not so wise, make it, and break it down..
//		if(isInstructionTime) {
//			instructionTime -= Time.deltaTime;
//			if(instructionTime < 0){
//				// clear instruction
//				isInstructionTime = false;
//				// start main game
//				isInQuiz = true;
//			}
//		}

		if(isInQuiz){
			
		}
		rotateTires ();
		//rotateFrontTiresOnTheYAxis ();
	}

	void checkCarPos(){
		//Debug.Log (transform.position.x);
	}

	void rotateTires(){
		this.transform.Find ("tire_1_front_left").transform.Rotate(0, Time.deltaTime * -900, 0);
		//this.transform.Find ("tire").transform.ro
		this.transform.Find ("tire_1_back_left").transform.Rotate(0, Time.deltaTime * -900, 0);
		this.transform.Find ("tire_3_front_right").transform.Rotate(0, Time.deltaTime * 900, 0);
		this.transform.Find ("tire_3_back_right").transform.Rotate(0, Time.deltaTime * 900, 0);
		tireCounter++;
	}

	void OnGUI() {
		
		//GUILayout.BeginArea (new Rect (0, 0, 100, 100));
		//string text = transform.position.x.ToString ();
		//string tt = Input.acceleration.x.ToString ();
		//tt = tt.Substring (0, 5);
		//GUILayout.Label (text, guistyle);
		//Debug.Log (transform.position.x);
	}

	// rotate tires on yaxis
	public static void rotateFrontTiresOnTheYAxisClockwise(){
		
		//GameObject.Find("tire_1_front_left").transform.Rotate(0, Time.deltaTime * 200, 0, Space.World);
		//GameObject.Find("tire_1_front_right").transform.Rotate(0, Time.deltaTime * 200, 0, Space.World);
		GameObject.Find("NEXCO_carmodel_0206_01").transform.Rotate(0, Time.deltaTime * 5, 0, Space.Self);
		//GameObject.Find("tire_1_front_left").transform.rotation.y = Time.deltaTime;
	}

	public static void rotateFrontTiresOnTheYAxisAntiClockwise(){
		//GameObject.Find ("tire_1_front_left").transform.Rotate (0, Time.deltaTime * -200, 0, Space.World);
		//GameObject.Find ("tire_1_front_right").transform.Rotate (0, Time.deltaTime * -200, 0, Space.World);
		GameObject.Find("NEXCO_carmodel_0206_01").transform.Rotate(0, Time.deltaTime * -5, 0, Space.Self);

	}
}
