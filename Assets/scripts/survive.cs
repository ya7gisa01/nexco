﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class survive : MonoBehaviour {
	// pass
	public static int score = 0;

	// pass level
	public static int selectedLevel = 0;

	// for debug
	public static int packageForDebug = 0;
	public Text text;

	// Use this for initialization
	void Awake () {
		//score += 10;
		// we want to pass this game object to next scene
		//DontDestroyOnLoad(this);
		// change score value here
		//score = 200;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	int getScore(){
		return score;
	}

	public void selectEasyLevel(){
		selectedLevel = 1;
	}

	public void selectNormalLevel(){
		selectedLevel = 2;
	}

	public void selectHardLevel(){
		selectedLevel = 3;
	}

	public void setDebugValue(){
		Debug.Log (text.text);
		packageForDebug = Convert.ToInt32(text.text);
	}
}
