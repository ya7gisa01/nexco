﻿using UnityEngine;
using System.Collections;
using System.IO;
//using UnityEditor;

public class VideoManager : MonoBehaviour {
	//public MovieTexture movie;
	//public AudioClip myAudioClip;
	//public AudioSource myAudioSrc;
	//FullScreenMovieScalingMode


	// Use this for initialization
	void Start () {
		//AudioSource myAudioSrc = null;
		//Time.timeScale = 2;
		//myAudioSrc.clip = movie.audioClip;
		//myAudioSrc.pitch = 50.0f; 

		//movie.Play ();
		//myAudioSrc.Play ();
		//Debug.Log (Time.timeScale);
		//Time.timeScale = 20.0f;
		//Debug.Log (AssetDatabase.GetAssetPath(movie));
		//string videoPath = AssetDatabase.GetAssetPath (movie);
		string movPath = "EasyMovieTexture.mp4";
		StartCoroutine (playMovie(movPath));
		// movie player for iOS
		Debug.Log (Handheld.PlayFullScreenMovie ("StreamingAssets/Proposed lower loop road flyby-HD.mp4", Color.black, FullScreenMovieControlMode.Full));
		Debug.Log("Hi there, we gonna play movie on iOS device.");
		//Handheld.PlayFullScreenMovie ("EasyMovieTexture.mp4", Color.black, FullScreenMovieControlMode.Hidden);
		//Handheld.PlayFullScreenMovie ("Proposed lower loop road flyby-HD.mp4", Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFill); 
	}
	
	// Update is called once per frame
	void Update () {


	}

	private IEnumerator playMovie(string url){
		//Time.timeScale = 10.0f;
		Handheld.PlayFullScreenMovie(url, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.AspectFill);
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();
	}
}
