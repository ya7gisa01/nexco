﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class Downloader : MonoBehaviour {

	string url = "http://dev.naked-inc.com/_inc/_sqlite/nexco.db3";
	//public static WWW worldWWW = null;
	public static bool isAndroidWWW = false;
	public static byte[] bytesAndroid;

	// Use this for initialization
	void Start () {
		// test method to dawnload png file.
		sendGetRequest();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private IEnumerator TestDownload(WWW www){
		//yield return www;
		// already method has got the WWW
		while (!www.isDone) {
			Debug.Log("downloaded " + (www.progress*100.0).ToString() + "%...");
			yield return null;
		}

		// set path to download item.
		string fullPath = Application.persistentDataPath + "/nexco.db3";
		//string fullPathB = Application.dataPath + "/screenshot.png";
		//string textAssetPath = Application.streamingAssetsPath + "/sampleText.txt";
		//string fullPath = "";
		//while (!www.isDone) { }
		if (www.isDone) {
			#if UNITY_EDITOR
			string editorPath = Application.dataPath + "/StreamingAssets/nexco.db3";
			// first, delete file..
			File.Delete(editorPath);
			File.WriteAllBytes(editorPath, www.bytes);

			#elif UNITY_ANDROID
			/*
			enough code
			*/
			// swap boolean
			isAndroidWWW = true;
			// assign value to byte[]
			bytesAndroid = www.bytes;
			/*
			enough code end
			*/



			// Android
			//string oriPath = System.IO.Path.Combine("jar:file://" + Application.dataPath + "!/assets/", "nexco.db3");

			// Android only use WWW to read file
			//WWW reader = new WWW(oriPath);
			//while (!reader.isDone) {}

			//string realPath = Application.persistentDataPath + "/nexco.db3";
			//System.IO.File.WriteAllBytes(oriPath, www.bytes);


			/*
			how about this?
			*/
			//string DatabaseName = "nexco.db3";
			//var filepath = string.Format("{0}/{1}", Application.persistentDataPath, DatabaseName);
			//string androidPath = "jar:file://" + Application.dataPath + "!/assets/" + DatabaseName;
			//var loadDb = new WWW(androidPath);  // this is the path to your StreamingAssets in android
			//while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			//File.Delete(androidPath);
			//File.WriteAllBytes(androidPath, www.bytes);
			/*
			end of how about this?
			*/

			string dbName = "nexco.db3";
			var pathDB = System.IO.Path.Combine (Application.persistentDataPath, dbName);
			//original path
			string sourcePath = System.IO.Path.Combine (Application.streamingAssetsPath, dbName);

			// Android  
			WWW wwwA = new WWW (sourcePath);
			// Wait for download to complete - not pretty at all but easy hack for now 
			// and it would not take long since the data is on the local device.
			while (!wwwA.isDone) {}

			if (String.IsNullOrEmpty(wwwA.error)) {                  
			System.IO.File.WriteAllBytes(pathDB, wwwA.bytes);
			}

			#endif
		}

		if (www.error != null) {
			#if UNITY_ANDROID
			isAndroidWWW = false;
			#endif
		}

		//Debug.Log (fullPath);
		//File.WriteAllBytes (fullPathB, www.bytes);
		//www.bytes;
		Debug.Log("download down!");
	}

	/*
	 * www thing
	 * methods to handle with data
	 */
	void sendGetRequest(){
		// just comment out below. we don't need to use header anymore.
		/*
		WWWForm form = new WWWForm ();
		form.AddField ("Username", "naked");
		form.AddField ("Password", "57905191");

		var headers = form.headers;
		string secretKey = "naked" + ":" + "57905191";
		headers["Authorization"]="Basic " + System.Convert.ToBase64String(
			System.Text.Encoding.ASCII.GetBytes(secretKey));


		// in case request is GET not POST, the second parameter could be null.
		WWW www = new WWW(url, null, headers);
		*/

		// final url
		string localUrl = "http://192.168.10.2/_inc/_sqlite/nexco.db3";
		WWW www = new WWW(localUrl);

		//yield
		StartCoroutine(TestDownload(www));
	}
}
