﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class showresultimg : MonoBehaviour {

	private GameObject targetImageGameObj;
	private Color transparentColor = new Color(1, 1, 1, 0);
	private Color clearColor = new Color(1, 1, 1, 1);
	private float fadeStartTime;

	public AudioClip fanfare0;
	public AudioClip fanfare20;
	public AudioClip fanfare40;
	public AudioClip fanfare60;
	public AudioClip fanfare80;
	public AudioClip fanfare100;

	AudioSource audio;
	// Use this for initialization
	void Start () {
		// get audio source component
		audio = GetComponent<AudioSource>();
		fadeStartTime = Time.fixedTime;
		showResultImg ();
	}
	
	// Update is called once per frame
	void Update () {
		// lerp alpha value
		targetImageGameObj.GetComponent<Image>().color = Color.Lerp(transparentColor, Color.white, Time.fixedTime - fadeStartTime);
	
	}

	void showResultImg(){
		int score = survive.score;
		if (score == 0) {
			GameObject.Find ("score_0").transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			// assign value to targetImageGameObj
			targetImageGameObj = GameObject.Find("score_0");
			// play shot
			audio.PlayOneShot(fanfare0, 1.0f);
		} else if (score == 20) {
			GameObject.Find ("score_20").transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			targetImageGameObj = GameObject.Find("score_20");
			// play shot
			audio.PlayOneShot(fanfare20, 1.0f);
		} else if (score == 40) {
			GameObject.Find ("score_40").transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			targetImageGameObj = GameObject.Find("score_40");
			// play shot
			audio.PlayOneShot(fanfare40, 1.0f);
		} else if (score == 60) {
			GameObject.Find ("score_60").transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			targetImageGameObj = GameObject.Find("score_60");
			// play shot
			audio.PlayOneShot(fanfare60, 1.0f);
		} else if (score == 80) {
			GameObject.Find ("score_80").transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			targetImageGameObj = GameObject.Find("score_80");
			// play shot
			audio.PlayOneShot(fanfare80, 1.0f);
		} else if (score == 100) {
			GameObject.Find ("score_100").transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			targetImageGameObj = GameObject.Find("score_100");
			// play shot
			audio.PlayOneShot(fanfare100, 1.0f);
		}
	}
}
